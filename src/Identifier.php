<?php

declare(strict_types=1);

namespace Danjones000\WhatAmI;

use Throwable;
use Illuminate\Broadcasting\BroadcastEvent;
use Illuminate\Support\Collection;

class Identifier
{
    protected static $getSub = [];
    protected static $initialized = false;

    /**
     * Constructor.
     */
    public function __construct()
    {
        if (!static::$initialized) {
            static::init();
        }
    }

    /**
     * Initialize subtype callables.
     */
    protected static function init(): void
    {
        foreach([
            'array',
            Collection::class,
            BroadcastEvent::class,
            Throwable::class
        ] as $type) {
            $baseClass = static::getBaseClass($type);
            static::$getSub[$type] = [static::class, "get{$baseClass}Sub"];
        }
    }

    /**
     * Get's the first item of an array.
     *
     * @param  array $what
     * @return mixed
     */
    protected static function getArraySub(array $what)
    {
        return count($what) ? reset($what) : null;
    }

    /**
     * Gets the first item of a Laravel Collection.
     *
     * @param  Collection $what
     * @return mixed
     */
    protected static function getCollectionSub(Collection $what)
    {
        return $what->count() > 0 ? $what->first() : null;
    }

    /**
     * Gets the event from a Laravel BroadcastEvent.
     *
     * @param  BroadcastEvent $what
     * @return mixed
     */
    protected static function getBroadcastEventSub(BroadcastEvent $what)
    {
        return is_null($what->event) ? null : $what->event;
    }

    /**
     * Gets the type of the previous Throwable for a given Throwable
     *
     * @param  Throwable $what
     * @return Throwable The previous throwable
     */
    protected static function getThrowableSub(Throwable $what): ?Throwable
    {
        return $what->getPrevious();
    }

    /**
     * Identifies $what.
     *
     * @param  mixed  $what
     * @param  bool   $useBaseClass If true, will only return the basename of the class
     * @param  bool   $skipSubType  If true, will not get subtypes
     * @return string
     */
    public function whatAmI($what, bool $useBaseClass = false, bool $skipSubType = false): string
    {
        $type = gettype($what);
        if ($type === 'object') {
            $class = $useBaseClass ? $this->getBaseClass($what) : get_class($what);

            if (!$skipSubType && !is_null($subType = $this->getSubType($what, $useBaseClass))) {
                return "{$class}<{$subType}>";
            }

            return $class;
        }

        if (!$skipSubType && ($type === 'array') && !is_null($subType = $this->getSubType($what, $useBaseClass))) {
            return "{$subType}[]";
        }

        return $type;
    }

    /**
     * Returns the base class for the object.
     *
     * @param  object|string $what
     * @return string
     */
    public static function getBaseClass($what): string
    {
        $class = is_string($what) ? $what : get_class($what);
        $pos = strrpos($class, '\\');
        if ($pos === false) {
            return $class;
        }

        return substr($class, $pos + 1);
    }

    /**
     * Gets the subtype of $what.
     *
     * @param  mixed  $what
     * @param  bool   $useBaseClass If true, will only return the basename of the class
     * @return string
     */
    public function getSubType($what, bool $useBaseClass = false): ?string
    {
        $baseType = $this->whatAmI($what, true, true);

        if (isset(static::$getSub[$baseType])) {
            return !is_null($sub = static::$getSub[$baseType]($what)) ? $this->whatAmI($sub, $useBaseClass) : null;
        }

        foreach (static::$getSub as $type => $cb) {
            if (is_a($what, $type) && !is_null($sub = $cb($what))) {
                return $this->whatAmI($sub, $useBaseClass);
            }
        }

        return null;
    }

    /**
     * Adds a callback to get a subtype.
     *
     * @param string   $class    The class that contains the subtype.
     * @param callable $callback The function that should return the subtype.
     *                           This function should accept a single argument, which is an instance of $class.
     *                           It should return the inner value (not the type), or `null` if none is found.
     */
    public static function addGetSub(string $class, callable $callback): void
    {
        static::$getSub[$class] = $callback;
    }
}
