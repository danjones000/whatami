<?php

declare(strict_types=1);

namespace Tests;

use Danjones000\WhatAmI\Identifier;
use PHPUnit\Framework\TestCase;

class IdentifierTest extends TestCase
{
    /** @var Identifier */
    protected $id;

    /**
     * @before
     */
    public function setupId(): void
    {
        $this->id = new Identifier();
    }

    public function providePrimitives(): array
    {
        return [
            [[], 'array'],
            [42, 'integer'],
            [true, 'boolean'],
            [null, 'NULL'],
            [42.0, 'double'],
        ];
    }

    /**
     * @dataProvider providePrimitives
     */
    public function testPrimitives($val, string $type): void
    {
        $this->assertEquals($type, $this->id->whatAmI($val));
    }

    public function provideArrays(): array
    {
        $prims = $this->providePrimitives();
        $arrays = array_map(function (array $vals) {
            $return = is_null($vals[0]) ? 'array' : $vals[1] . '[]';
            return [[$vals[0]], $return];
        }, $prims);
        $arrays[] = $prims[0];

        return $arrays;
    }

    /**
     * @dataProvider provideArrays
     */
    public function testArrays(array $val, string $type): void
    {
        $this->testPrimitives($val, $type);
    }

}
