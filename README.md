# What Am I?

This is a php package that has a single class, which provides methods for determining what type of thing a variable is.

The idea behind this is that when debugging data, sometimes you don't know exactly what a thing is that was passed. You
can call this method to find out what that thing is.

It also includes some limited sub-typing. E.g., for an empty array, it will tell you that the type is `array`. However,
if there are items in the array, it will look at the first element, and return the type of that element, e.g., if the
first element is an integer, it will return `integer[]`.

## Installation

Add the following to your `composer.json`:

``` json
"repositories": [
    {
        "type":"vcs",
        "url":"git@gitlab.com:danjones000/whatami.git"
    }
]
```

Then run:

``` shell
composer require danjones000/whatami:dev-master
```

I'll add it to Packagist once it's stable, then you can switch to a specific version.

## Usage

This package is most useful in debugging situations, or in exception messages. E.g., consider the following situation:

``` php
function doSomething($subject) {
    if (!is_array($subject) && !is_string($subject)) {
        throw new InvalidArgumentException('doSomething must have a string or array');
    }

    // do something
}
```

You could improve your error logs, by indicating what was actually passed. E.g.:

``` php
use Danjones000\WhatAmI\Identifier;
use InvalidArgumentException;

function doSomething($subject) {
    if (!is_array($subject) && !is_string($subject)) {
        $ident = new Identifier();
        throw new InvalidArgumentException('doSomething must have a string or array.' . $ident->whatAmI($subject) . 
            'passed instead');
    }

    // do something
}
```

### Advanced Usage

#### Basenames for classes

If you don't want the fully-qualified classname, you can have it return the shortened form:

``` php
namespace Vendor\Domain;

use Danjones000\WhatAmI\Identifier;

class Thing { }

$t = new Thing();
$ident = new Identifier();
$ident->whatAmI($t); // => Vendor\Domain\Thing
$ident->whatAmI($t, true); // => Thing
```

#### Subtypes

`Identifier` can also identify subtypes for a limited number of types. E.g., if passed an empty array, `whatAmI` will
return `array`. However, if there are elements to the array, the first element is seen as the subtype, and that is
returned. If the first element is an integer, for example, instead of `array`, it returns `integer[]`.

For any other supported types, a "generics" type format is used. E.g., Laravel collections are supported in the same way
that arrays are:

``` php
use Carbon\Carbon;
use Danjones000\WhatAmI\Identifier;
use Illuminate\Support\Collection;

$coll = Collection::make([42]);
$ident = new Identifier();
$ident->whatAmI($coll); // => Illuminate\Support\Collection<integer>

// subtypes are also recursive
$coll = Collection::make([[Carbon::now()]]);
$ident->whatAmI($coll); // => Illuminate\Support\Collection<Carbon\Carbon[]>
```

Additional types can be added. E.g.:

``` php
use Danjones000\WhatAmI\Identifier;

class Container {
    public $thing;
}

Identifier::addGetSub(Container::class, function (Container $what) {
    return $what->thing;
});

$c = new Container();
$c->thing = new DateTime();
$ident = new Identifier();
$ident->whatAmI($c); // => Container<DateTime>
```

You can also skip checking for subtypes entirely.

``` php
use Danjones000\WhatAmI\Identifier;

$ident = new Identifier();
$ident->whatAmI([42]); // integer[]
$ident->whatAmI([42], false, true); // => array
```

## Roadmap / Todo

- Separate Laravel classes to separate library, with Laravel service provider
- Add to packagist
- git-flow
    + clarify versioning
- CHANGELOG
